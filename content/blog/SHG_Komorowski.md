---
title: "Integralność gospodarowania nowym paradygmatem nauk ekonomicznych"
date: 2023-04-13T12:49:27.000+06:00
featureImage: images/blog/SGH_Warsaw.jpg
tags:
- Economia
categories:
- Wydarzenia naukowe
---

Prof. dr hab. Jan Komorowski z SGH w Warszawie przedstawił wykład Pt.: '\'Integralność gospodarowania nowym paradygmatem nauk ekonomicznych'\'
