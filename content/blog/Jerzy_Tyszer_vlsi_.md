---
title: "Wykład '\'VLSI Test'\' Prof. Jerzy Tyszera z Politechniki Poznańskiej"
date: 2022-07-13T12:49:27+06:00
featureImage: images/blog/Politechnika_2020.jpg
tags:
- VLSI Test
categories:
- Nasze spotkania
---

Prof. dr hab. inż. Jerzy Tyszer z Politechniki Poznańskiej przedstawił 40 minutowy wykład '\'VLSI Test'\'. W trakcie seminarium zaprezentowano stan wiedzy dotyczący złożoności układów scalonych, które zawarte są w większości urządzeń z których korzystamy. Omówiono problematykę ich testowania w kontekście niezawodności.