---
title: "Challenges in low cost air quality sensor data modeling"
date: 2023-02-13T12:49:27+06:00
featureImage: images/blog/Aristotelian_University.jpg
tags:
- DATA
categories:
- Nasze spotkania
---

Prof. Kostasa Karatzasa z Uniwersytetu Arystotelesa w Salonikach przedstwił wykład 
pt.: '\'Challenges in low cost air quality sensor data modeling'\'
