---
title: "Czym jest sztuczna inteligencja i jakie są perspektywy jej rozwoju"
date: 2022-11-13T13:06:26+06:00
featureImage: images/blog/ryszard2.jpg
tags:
- AI
categories:
- Nasze spotkania
---

Profesor Ryszard Tadeusiewicz, wybitny polski informatyk specjalizujący się w obszarze sztucznej inteligencji, autor ponad stu książek i ponad tysiąca artykułów naukowych, doktor honorowy kilkunastu zagranicznych i krajowych uczelni, były wielokrotny rektor AGH, mistrz mowy polskiej i zaangażowany popularyzator nauki wygłosił wykład gościnny pt.: '\'Czym jest sztuczna inteligencja i jakie są perspektywy jej rozwoju'\'.