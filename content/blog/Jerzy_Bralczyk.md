---
title: "Jak wybić się na wybitność"
date: 2024-05-14T12:49:27+06:00
featureImage: images/blog/news_Jerzy_Bralczyk.jpg
tags:
- Jerzy Bralczyk
categories:
- Nasze spotkania
---

Gościem specjalnym tej inspirującej rozmowy był profesor Jerzy Bralczyk, wybitny polski językoznawca, profesor nauk humanistycznych, znany z pracy nad poprawnością językową oraz działalnością na rzecz kultury języka polskiego. Spotkanie odbyło się 14 maja o godzinie 11:30, w auli C004, w budynku C Uniwersytetu WSB Merito w Gdańsku.

Był to czas wyjątkowej rozmowy, która z pewnością była nie tylko inspirująca, ale także pełna ciekawych refleksji i nowych spojrzeń na temat osiągania wybitności przy pełnej auli i wielu pytaniach kierowanych do Pana Profesora.