---
title: "Filary sukcesu przedsiębiorczych organizacj"
date: 2023-06-13T12:49:27+06:00
featureImage: images/blog/kato.jpg
tags:
- Economics
categories:
- Nasze spotkania
---

### Prof. Wojciech Dyduch z Uniwersytetu Ekonomicznego w Katowicach i Przewodniczący Komitetu Nauk Organizacji i Zarządzania Polskiej Akademii Nauk przedstawił wykład pt. '\'Filary sukcesu przedsiębiorczych organizacji'\'

'\'Profesor W. Dyduch jest pracownikiem Uniwersytetu Ekonomicznego w Katowicach. Pełni też funkcję Przewodniczącego Komitetu Nauk Organizacji i Zarządzania Polskiej Akademii Nauk. W trakcie wykładu profesor W. Dyduch  w sposób ciekawy, odwołując się do licznych przykładów i zaprezentował kluczowe elementy i cechy stanowiące filary przedsiębiorczości organizacyjnej, nie tylko przedsiębiorstwa przedsiębiorczego ale również na przykładzie Uniwersytetu WSB Mderito - przedsiębiorczego  uniwersytetu. W zakresie przedstawionego problemu odbyła się po wykładzie, pogłębiająca treść wykładu  ciekawa dyskusja.'\'