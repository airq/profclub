---
# preview details
title: Prof. dr hab. inż. Cezary Orłowski
category: 
category_slug: 
image: images/works/Cezary Orłowski.jpg

# full details
full_image: images/works/Cezary Orłowski.jpg
info:
  - label: Year
    value: 2023.06

  - label: Economics
    value:  

description1:
  enable: true
  title: 
  text: "Profesor informatyki technicznej. Jest dyrektorem IBM Center for Advanced Studies on Campus w Uniwersytecie WSB Merito w Gdańsku. Uczestniczył w wielu projektach informatycznych (m.in. dla banków, dużych firm informatycznych i miast), kierując projektami transformacji cyfrowych organizacji oraz będąc doradcą w zakresie doboru technologii oraz procesów przetwarzania Big Data. W projektach IBM kierował procesami integracji usług dla IBM Watson. Obecnie wraz z zespołem zajmuje się projektowaniem systemów Internetu Rzeczy z wykorzystaniem metod generatywnej sztucznej inteligencji dla Smart Cities."




---
