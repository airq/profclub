---
# preview details
title: dr hab. Marian Turek Prof. Uniwersytetu WSB Merito
category: 
category_slug: 
image: images/works/marian_turek_.jpg

# full details
full_image: images/works/marian_turek_.jpg
info:
  - label: Year
    value: 2023.06

  - label: Economics
    value:  

description1:
  enable: true
  title: 
  text: "Marian Turek Prowadził zajęcia z polityki gospodarczej, ekonomii, ekonomiki turystyki, prognozowania społeczno-ekonomicznego, ekonomii wyboru publicznego, Increase in Wealth in Western Europe, Milton Friedman’s Liberal Economics, Nobel Prize in Economic Sciences, Nobel Prize in Economic Sciences i inn. Wypromował około 800 magistrantów a taakże 8 doktorantów. Pełnił m.in. funkcje: kierownika Podyplomowego Studium dla Nauczycieli Przedmiotów Ekonomicznych UG, kierownika Międzywydziałowego Studium Pedagogicznego UG (1991 – 1995), Dyrektor Ośrodka Edukacji Ekonomicznej i Obywatelskiej przy UG (1993 – 1995), za-cy dyrektora Instytutu Teorii Ekonomii UG 1990 – 1996, kierownika Studium Podyplomowego Przedsiębiorczości UG (2001 – 2012). Odbył kilka staży zagranicznych, m.in.: Ecole des Hautes Etudes en Sciences Sociales, Paryż; Nederlandsch Economisch Instituut, Rotterdam; Ecole des Hautes Etudes en Sciences Commerciales, Cergy Pontoise; College of Further Education, Leeds. Były członek organizacji: PTE, TUP, GTPS i in. Autor wielu opracowań profesjonalnych, a także publikacji: Hevelius and his Gdańsk, GTN, Gdańsk 2013."




---
