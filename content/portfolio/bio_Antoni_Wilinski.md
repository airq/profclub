---
# preview details
title: Profesor dr hab. inż. Antoni Wiliński
category: 
category_slug: 
image: images/works/Antoni.jpg

# full details
full_image: images/works/Antoni.jpg
info:
  - label: Year
    value: 2023.05

  - label: Programowanie
    value:  

description1:
  enable: true
  title: 
  text: "<p>Rocznik 1947, studiował w Wyższej Szkole Marynarki Wojennej w Gdyni w latach 1965-69 na kierunku elektromechanika, na Politechnice Gdańskiej budownictwo okrętowe na Wydziale Budowy Okrętów w latach 1971-73.
Doktorat w Akademii Marynarki Wojennej w Leningradzie w latach 1974-76 w zakresie teorii systemów; habilitacja na Wydziale Mechaniki Energetyki i Lotnictwa Politechniki Warszawskiej w 1989 w zakresie teorii sterowania.
Tytuł profesora otrzymał z rąk Prezydenta RP w październiku 2011 roku.
Uczelnie:
- Uniwersytet WSB Merito w Gdańsku od 2020 na Wydziale Informatyki i Nowych Technologii;
- profesor zwyczajny Politechnika Koszalińska; Wydział Elektroniki i Informatyki; Katedra Inżynierii Komputerowej od 2018;
- profesor nadzwyczajny: Zachodniopomorski Uniwersytet Technologiczny w Szczecinie; Wydział Informatyki; Katedra Systemów Multimedialnych 2009-2018; dziekan w latach 2009-2016;
- profesor: Politechnika Szczecińska; Wydział Informatyki; Instytut Grafiki Komputerowej i Systemów Multimedialnych 1992-2008; dziekan w 2008 roku;
- profesor: Państwowa Wyższa Szkoła Zawodowa w Elblągu; Instytut Informatyki Stosowanej 2000-2003;
- profesor: Wyższa Informatyczna Szkoła Zawodowa w Gorzowie Wielkopolskim 2005-2008;
       Pełnione funkcje:
- Koordynator badań naukowych w naukach technicznych w WSB Merito Gdańsk;
- Dziekan: Zachodniopomorski Uniwersytet Technologiczny w Szczecinie; Wydział Informatyki, 2009-2016
- Dziekan: Politechnika Szczecińska; Wydział Informatyki 2008
- Kierownik: Zachodniopomorski Uniwersytet Technologiczny w Szczecinie; Wydział Informatyki; Katedra Systemów Multimedialnych 2009-2012
- Kierownik: Politechnika Szczecińska; Wydział Informatyki; Katedra Systemów Multimedialnych 2006-2008
- Dyrektor: Państwowa Wyższa Szkoła Zawodowa w Elblągu; Instytut Informatyki Stosowanej 2000-2003
Obszary badań naukowych:
Od 1995 roku: informatyka, uczenie maszynowe, rozpoznawanie wzorców, eksploracja danych i wiedzy, sztuczna inteligencja i jej zastosowania, prognozowanie i modelowanie, modelowanie predykcyjne – 93 prac badawcze w formie książek i artykułów; 9 wypromowanych doktorów informatyki.
Do 1995 roku: oceanotechnika, sterowanie ruchem obiektów podwodnych, roboty i manipulatory podwodne, pojazdy podwodne, ROV – 110 prac badawczych w formie książek, skryptów, artykułów i projektów n-b; 1 wypromowany dr nauk technicznych.</p>"




---
