---
# preview details
title: profesor Marcin Kalinowski
category:  
category_slug:  
image: images/works/merito.jpg

# full details
full_image: images/works/merito.jpg
info:
  - label: Year
    value: 2022.11

  - label: Technology
    value: AI

description1:
  enable: true
  title: 
  text: "- Doktor habilitowany w zakresie dyscypliny Finanse, Profesor Uniwersytetu WSB Merito w Gdańsku
- Autor kilkudziesięciu publikacji naukowych z zakresu finansów (Rynki finansowe, inwestycje finansowe, finanse behawioralne, zarzadzanie ryzykiem finansowym)
- Odbył wiele staży w zagranicznych Uczelniach w tym w New York University Stern School of Business w Stanach Zjednoczonych 
- Prezydent Federacji Naukowej WSB-DSW MERITO (Instytucji naukowej stworzonej przez 5 Uniwersytetów – Uniwersytet WSB Merito w Gdańsku, Poznaniu, Wrocławiu oraz Uniwersytet Dolnośląski DSW we Wrocławiu)
- Były Dziekan Wydziału Finansów I Zarządzania Wyższej Szkoły Bankowej w Gdańsku w latach 2007-2018, w tym czasie
- Kierownik Katedry Finansów Wyższej Szkoły Bankowej w Gdańsku
- Przewodniczący studiów MBA WSB w Gdańsku i Northampton University (UK)
- Wykładowca Akademii Leona Koźmińskiego w Warszawie
- Były wykładowca Helsinki School of Business
- Były Członek Rady Nadzorczej firmy Leier Markowicze S.A.
- Były Prezes gdańskiego oddziału Towarzystwa Naukowego Organizacji i Kierownictwa
- Wieloletni praktyk życia gospodarczego - pracował w wielu firmach o zasięgu międzynarodowym (polskich, niemieckich, fińskich: Grupa Organika, Assmann GMBH, Martela Oy, uczestnicząc w wielu projektach rozwojowych i inwestycyjnych."




---
