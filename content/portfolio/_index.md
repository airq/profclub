---
title: Członkowie klubu
text: "<p>Nasza społeczność profesorska to zróżnicowany zespół specjalistów w różnych dziedzinach, są autorami setek fundamentalnych prac naukowych. Nieustannie się rozwijamy, ponieważ chcemy inspirować i przekazywać wiedzę, która ma realny wpływ na nasz świat.</p>"
---
