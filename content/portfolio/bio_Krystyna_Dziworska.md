---
# preview details
title: Prof. dr hab. Krystyna Dziworska 
category: 
category_slug: 
image: images/works/krystyna_dziworska_.jpg

# full details
full_image: images/works/krystyna_dziworska_.jpg
info:
  - label: 
    value: 

  - label: 
    value:  

description1:
  enable: true
  title: 
  text: "<p>Prof. dr hab. Krystyna Dziworska jest Autorką licznych publikacji naukowych i prac badawczych z zakresu zarządzania finansami, inwestycjami, i nieruchomościami. Jest również promotorem wielu prac doktorskich, recenzentem prac habilitacyjnych i postępowań profesorskich w przedmiotowym obszarze wiedzy. Posiada wieloletnie doświadczenie zawodowe w nadzorze właścicielskim spółek prawa handlowego z: sektora chemicznego, przemysłu lekkiego, ubezpieczeń oraz gospodarki komunalnej. Wykazuje zaangażowanie w wielu działaniach naukowych, organizacyjnych i dydaktycznych kreujących rozwój rodzimego rynku nieruchomości.</p>"




---
