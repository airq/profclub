---
# preview details
title: Prof. dr hab. dr h.c. multi Bogdan Nogalski
category:  
category_slug:  
image: images/works/bogdan_nogalski.jpg

# full details
full_image: images/works/bogdan_nogalski.jpg
info:
  - label: Year
    value: 2022.06

  - label: Technology
    value: VLSI Test 

description1:
  enable: true
  title: 
  text: "<p>Profesor tytularny nauk ekonomicznych w zakresie organizacji i zarządzania. Obecnie profesor zwyczajny w Wyższej Szkole Bankowej w Gdańsku. Wcześniej w latach 1973-2018 pracownik badawczo-dydaktyczny Uniwersytetu Gdańskiego. Doktor honoris causa Uniwersytetu Biottera w Bukareszcie [2004], Politechniki Śląskiej w Gliwicach [2016] i Uniwersytetu Ekonomicznego w Katowicach [2022]. Zajmuje się problemami zarządzania organizacjami oraz doskonalenia różnorodnych form organizacji i systemów zarządzania. Specjalnością naukową jest problematyka dotycząca różnorodnych aspektów zarządzania strategicznego. Autor wielu oryginalnych – z tego zakresu - prac naukowo badawczych, aplikacyjnych oraz ekspertyz. Pełni i pełnił odpowiedzialne funkcje w środowisku naukowym. Jest Honorowym Przewodniczącym Komitetu Nauk Organizacji i Zarządzania Polskiej Akademii Nauk. W latach 2003-2020 był jego Przewodniczącym. W latach 2007-2016 był członkiem Sekcji Nauk Ekonomicznych Centralnej Komisji do Spraw Stopni i Tytułów, a w latach 2013–2016 jej Przewodniczącym i członkiem Prezydium tej Komisji.</p>"




---
